package info.dhickcunk.cobapw2.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

import info.dhickcunk.cobapw2.DetailActivity;
import info.dhickcunk.cobapw2.R;
import info.dhickcunk.cobapw2.MainActivity;
import info.dhickcunk.cobapw2.utils.Constrants;

public class PostingAdapter extends RecyclerView.Adapter<PostingAdapter.ViewHolder> {

    private ArrayList<HashMap<String, String>> postList;
    private MainActivity mainActivity;
    View viewRoot;

    public PostingAdapter(ArrayList<HashMap<String, String>> postList, MainActivity mainActivity) {
        this.postList = postList;
        this.mainActivity = mainActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_buku, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final HashMap<String, String> post = postList.get(position);

        Glide.with(mainActivity)
                .load(Constrants.IMAGE_URL+post.get("cover"))
                .placeholder(R.mipmap.ic_launcher)
                .into(viewHolder.imgCover);
        viewHolder.textJudul.setText(post.get("judul_buku"));
        viewHolder.textPengarang.setText(post.get("pengarang"));
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgCover;
        TextView textJudul;
        TextView textPengarang;

        public ViewHolder(View view) {
            super(view);
            imgCover = (ImageView) view.findViewById(R.id.imgCover);
            textJudul = (TextView) view.findViewById(R.id.textJudul);
            textPengarang = (TextView) view.findViewById(R.id.textPengarang);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final HashMap<String, String> post = postList.get(getAdapterPosition());

            Intent intent = new Intent(mainActivity, DetailActivity.class);
            intent.putExtra("map", post);
            mainActivity.startActivity(intent);

            Log.e("posisi","-> "+getAdapterPosition()) ;
        }
    }
}
