package info.dhickcunk.cobapw2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;

import info.dhickcunk.cobapw2.utils.Constrants;

public class DetailActivity extends AppCompatActivity {
    ImageView imgCover;
    TextView judulText, pengarangText, penerbitText, tahunTerbitText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        imgCover = (ImageView)findViewById(R.id.coverDetail);
        judulText = (TextView)findViewById(R.id.judulBukuDetail);
        pengarangText = (TextView)findViewById(R.id.pengarangDetail);
        penerbitText = (TextView)findViewById(R.id.penerbitDetail);
        tahunTerbitText = (TextView)findViewById(R.id.tahunTerbitDetail);

        Intent intent = getIntent();
        HashMap<String, String> hashMap = (HashMap<String, String>)intent.getSerializableExtra("map");

        Glide.with(this)
                .load(Constrants.IMAGE_URL+hashMap.get("cover"))
                .placeholder(R.mipmap.ic_launcher)
                .into(imgCover);
        judulText.setText(hashMap.get("judul_buku"));
        pengarangText.setText(hashMap.get("pengarang"));
        penerbitText.setText(hashMap.get("penerbit"));
        tahunTerbitText.setText(hashMap.get("tahun_terbit"));
    }
}
