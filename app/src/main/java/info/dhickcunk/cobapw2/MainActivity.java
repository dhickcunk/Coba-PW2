package info.dhickcunk.cobapw2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.dhickcunk.cobapw2.adapter.PostingAdapter;
import info.dhickcunk.cobapw2.utils.Constrants;
import info.dhickcunk.cobapw2.utils.CustomHttpClient;

public class MainActivity extends AppCompatActivity {
    private RecyclerView listPosting;
    LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TambahBukuActivity.class));
            }
        });

        listPosting = (RecyclerView) findViewById(R.id.listPosting);
        linearLayoutManager = new LinearLayoutManager(this);
        listPosting.setLayoutManager(linearLayoutManager);
        new GetBooksAsyncTask().execute() ;
    }

    private class GetBooksAsyncTask extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String respon = "";
            try {
                String url = Constrants.URL_GET_BUKU;
                respon = CustomHttpClient.executeHttpGet(url);
            } catch (Exception e) {
                respon = e.toString();
            }
            return respon;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            try {
                Log.e("masuk","HomeFragment result -> "+result) ;
                JSONObject object = new JSONObject(result) ;
                ArrayList<HashMap<String, String>> arr = new ArrayList<>();
                if(object.getString("success").equalsIgnoreCase("1")){
                    JSONArray array = object.getJSONArray("data") ;
                    HashMap<String, String> map;
                    for (int i = 0; i < array.length(); i++){
                        JSONObject jsonObject = array.getJSONObject(i) ;
                        map = new HashMap<String, String>();
                        map.put("id_buku", jsonObject.getString("id_buku"));
                        map.put("judul_buku", jsonObject.getString("judul_buku"));
                        map.put("tahun_terbit", jsonObject.getString("tahun_terbit"));
                        map.put("penerbit", jsonObject.getString("penerbit"));
                        map.put("cover", jsonObject.getString("cover"));
                        map.put("pengarang", jsonObject.getString("pengarang"));
                        arr.add(map);
                    }
                }
                listPosting.setAdapter(new PostingAdapter(arr, MainActivity.this));
            } catch (Exception e) {
                Log.e("masuk","-> "+e.getMessage()) ;
            }
        }
    }
}
